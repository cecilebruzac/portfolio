'use strict';

export default function displayInstagramFeed(accessToken, templatePath, containerId, callback) {
    const getTemplatePart = function (data) {
      return new Promise(function (resolve, reject) {
        const xhr = new XMLHttpRequest();
        xhr.open('POST', templatePath);
        xhr.responseType = 'document';
        xhr.onload = function () {
          if (xhr.status === 200) {
            resolve(xhr.response);
          } else {
            console.log('Request failed.  Returned status of ' + xhr.status);
            reject();
          }
        };
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.send('data=' + encodeURIComponent(JSON.stringify(data)));
      });
    };
  
    const xhr = new XMLHttpRequest();
    xhr.open(
      'GET',
      'https://graph.instagram.com/me/media?fields=caption,id,media_type,media_url,permalink,username,timestamp&access_token=' +
        accessToken
    );
  
    xhr.onload = function () {
      if (xhr.status === 200) {
        const data = JSON.parse(xhr.responseText).data;
        getTemplatePart(data).then(function (response) {
          const container = document.getElementById(containerId);
          const responseContent = response.getElementById(containerId);
          if (container && responseContent) {
            container.innerHTML = responseContent.innerHTML;
            callback();
          }
        });
      } else {
        console.log('Request failed.  Returned status of ' + xhr.status);
      }
    };

    xhr.send();

  }