'use strict';

require('../scss/main.scss');

import displayInstagramFeed from './displayInstagramFeed.js';
import cookieconsent from '../node_modules/cookieconsent/build/cookieconsent.min.js';

function playVideo(videoId) {
  const video = document.getElementById(videoId);
  video.play();
}

function removeElement(elementId, timing = 500, callback = null) {
  const element = document.getElementById(elementId);

  if (element) {
    element.style.setProperty('transition', 'opacity ' + (timing / 1000) + 's ease-in-out');
    element.style.setProperty('opacity', '0');

    setTimeout(function () {
      element.parentNode.removeChild(element);

      if (callback) {
        callback();
      }
    }, timing);
  }
}

(function () {
  document.body.classList.remove('no-js');
  
  window.playVideo = playVideo;
  window.removeElement = removeElement;

  window.cookieconsent.initialise({
    container: document.getElementById('content'),
    position: 'bottom-right',
    theme: 'block',
    content: {
      message: 'Ce site utilise Google Analytics. En continuant à naviguer, vous autorisez le dépôt de cookies à des fins de mesure&nbsp;d’audience.',
      dismiss: 'Ok',
      policy: 'Politique cookies'
    },
    palette:{
     popup: {
       background: '#7389ae', 
       text: '#fff', 
       link: '#fff'
      },
      button: {
        background: '#FFFFFF', 
        border: 'transparent', 
        text: '#4a4a4a'
      }
    },
    revokable: false,
    showLink: false,
    onStatusChange: function(status) {
     console.log(this.hasConsented() ? 'enable cookies' : 'disable cookies');
    },
    law: {
     regionalLaw: false,
    },
    location: true
   });

  const instagramFeedElement = document.querySelector('[data-instagram-token]');

  if(instagramFeedElement) {  
    function initLazzyLoading() {
      const imagesElements = document.querySelectorAll('img[data-src]');
      let index = 0;

      function lazzyloadInstagramImages(index) {
        if (index >= imagesElements.length) return;
        
        const imageElement = imagesElements[index];
        const downloadingImage = new Image();
        const imageSrc = imagesElements[index].getAttribute('data-src');
      
        downloadingImage.onload = function() {
          imageElement.setAttribute('src', imageSrc);
          imageElement.removeAttribute('data-src');
          imageElement.classList.remove('instagram-feed__picture--hidden');
          index++;
          lazzyloadInstagramImages(index);
        };

        downloadingImage.src = imageSrc;        
      }

      lazzyloadInstagramImages(index);
    }

    displayInstagramFeed(instagramFeedElement.dataset.instagramToken, instagramFeedElement.dataset.instagramFeedTemplate, instagramFeedElement.dataset.instagramFeedId, initLazzyLoading);
  }

})();
