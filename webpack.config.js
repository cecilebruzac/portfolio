const path = require('path');
const webpack = require('webpack');

const MiniCsssExtractPlugin = require('mini-css-extract-plugin');


module.exports = (env) => {    
    return [{
        entry: './scripts/main.js',
        output: {
            path: path.join(__dirname, './site/assets'),
            filename: '[name].js',
            publicPath: '/dist/'
        },
        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    use: {
                        loader: 'babel-loader',
                        options: {
                            presets: ['@babel/preset-env']
                        }
                    }
                },
                {
                    test: /\.scss$/,
                    use: [
                        {
                            loader: MiniCsssExtractPlugin.loader
                        },
                        { 
                            loader: 'css-loader'
                        },
                        {
                            loader: 'postcss-loader'
                        },
                        {
                            loader: 'sass-loader',
                            options: {
                                implementation: require('sass')
                            }
                        }
                    ]
                }
            ]
        },
        plugins: [
            new MiniCsssExtractPlugin({ filename: 'main.css'})
        ]
    }]
}