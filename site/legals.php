<?php
  require_once('config.php');
  require_once TEMPLATE_PARTS_PATH . 'head.tpl.php';
  $title = 'Mentions légales | Cécile Bruzac';
  $ogImagePath = '';
  displayHead($title, $ogImagePath);
?>

<header class="main-header">
  <p class="main-header__title text text--bold text--big"><a class="text__link"  href="/">Cécile Bruzac</a></p>
</header>
<div class="legals">
  <article class="legals__content row row--centered">
    <h1 class="legals__title text text--uppercase text--big text--bigger-md">Mentions légales</h1>
    <div class="row__col row__col--half-md row__col--with-right-gutter-md">
      <h2 class="legals__subtitle text text--bold text--uppercase text--medium">Éditeur</h2>
      <p class="text text--medium">Vous êtes actuellement connecté(e) sur le site de Cécile Bruzac, édité par Cécile Bruzac – 9 rue de Madagascar – 75012 Paris, France.</p>
      <p class="text text--medium">Contact&nbsp;: <a class="text__link"
                      href="mailto:&#099;&#111;&#110;&#116;&#097;&#099;&#116;&#064;&#099;&#101;&#099;&#105;&#108;&#101;&#098;&#114;&#117;&#122;&#097;&#099;&#046;&#102;&#114;">&#099;&#111;&#110;&#116;&#097;&#099;&#116;&#064;&#099;&#101;&#099;&#105;&#108;&#101;&#098;&#114;&#117;&#122;&#097;&#099;&#046;&#102;&#114;</a></p>
      <h2 class="legals__subtitle text text--bold text--uppercase text--medium">Conception</h2>
      <p class="text text--medium">Ce site a été conçu et développé par Cécile Bruzac.</p>
      <h2 class="legals__subtitle text text--bold text--uppercase text--medium">Hébergement</h2>
      <p class="text text--medium">GANDI SAS – 63, 65 Boulevard Massena – 75013 Paris, France<br>Téléphone&nbsp;: <a class="text__link" href="tel:+33170377661">+33170377661</a></p>
    </div><!--
    --><div class="row__col row__col--half-md row__col--with-left-gutter-md">
      <h2 class="legals__subtitle text text--bold text--uppercase text--medium">Droit d’auteur</h2>
      <p class="text text--medium">Le site cecilebruzac.fr et les contenus du site sont couverts par le droit de la propriété intellectuelle, notamment par les articles L.111-1, L.112-2 et L.341-1 du code de la propriété intellectuelle. Tous les droits d’exploitation sont réservés, y compris pour les documents téléchargeables et les représentations iconographiques et photographiques. Toute reproduction, représentation, utilisation ou modification, par quelque procédé que ce soit, de tout ou partie du site, de tout ou partie des différents documents qui le composent, sans avoir obtenu l’autorisation préalable de Cécile Bruzac est strictement interdite et constitue un délit de contrefaçon (article L.122-4 du code de la propriété intellectuelle).</p>
    </div>
    <p class="text text--big"><a class="text__link" href="/"> &larr; </a></p>
  </article>
</div>

<?php include TEMPLATE_PARTS_PATH . 'footer.tpl.php'; ?>