<?php
  require_once('config.php');
  require_once TEMPLATE_PARTS_PATH . 'head.tpl.php';
  $title = 'Instagram feed | Cécile Bruzac';
  $ogImagePath = '';
  displayHead($title, $ogImagePath);
?>

<header class="main-header">
  <h1 class="main-header__title text text--bold text--big"><a class="text__link" href="/">Cécile Bruzac</a></h1>
</header>

<?php if(!!$_POST && !!$_POST['data']) {
  $contents = $_POST['data'];
  $data = json_decode( $contents, true, 512, JSON_BIGINT_AS_STRING);
} else {
  $contents_link='https://graph.instagram.com/me/media?fields=caption,id,media_type,media_url,permalink,username,timestamp&access_token='.INSTAGRAM_ACCESS_TOKEN;
  $contents = file_get_contents($contents_link);
  $json = json_decode( $contents, true, 512, JSON_BIGINT_AS_STRING);
  $data = $json['data'];
}

include TEMPLATE_PARTS_PATH . 'instagram-feed.tpl.php'; ?>

<?php include TEMPLATE_PARTS_PATH . 'footer.tpl.php'; ?>