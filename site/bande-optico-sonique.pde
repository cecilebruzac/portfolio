import processing.video.*;
import gab.opencv.*;
import controlP5.*;
import themidibus.*;

Movie myMovie;
OpenCV opencv;
ControlP5 controlP5;
MidiBus midiBus;

int threshold = 100, xNote, pitch, channel;

ArrayList<Contour> contours;

void setup() {
  size(540, 900);
  
  myMovie = new Movie(this, "IMG_4988.mov");
  myMovie.loop();
  myMovie.volume(0);

  opencv = new OpenCV(this, 1080, 1920);

  controlP5 = new ControlP5(this);
  controlP5.addSlider("threshold").setPosition(20, height - 30).setRange(0, 255);

  xNote = width / 2;
  controlP5.addSlider("xNote").setPosition(200, height - 30).setRange(1, width - 1);

  MidiBus.list();
  midiBus = new MidiBus(this, 0, 1);
}

void draw() {

  background(255);

  opencv.loadImage(myMovie);
  opencv.gray();
  opencv.threshold(threshold);
  contours = opencv.findContours();

  noFill();
  strokeWeight(2);
  scale(0.5);

  for (Contour contour : contours) {
    stroke(255, 0, 0);
    contour.draw();
  }

  scale(2);

  for (int yNote = 0; yNote < height; yNote++) {
    color myColor = get(xNote, yNote);
    float r = red(myColor);
    float g = green(myColor);

    if ( r == 255 && g == 0 && yNote > 0 ) {
      filter(GRAY);
      fill(0, 0, 0);
      noStroke();
      ellipse(xNote, yNote, 15, 15);

      pitch= 127-((127*yNote)/height);

      if (pitch > 84) channel = 0; //aigus
      if (pitch < 84 && pitch > 42) channel = 1; //mediums
      if (pitch < 42) channel = 2; //graves

      midiBus.sendNoteOn(channel, pitch, 10);
      midiBus.sendNoteOff(channel, pitch, 10);      

      break;
    }
  }

  noFill();
  stroke(255);
  rect(0, 0, width, height);

  fill(0, 0, 0, 100);
  noStroke();
  rect(0, height - 50, width, 50);
}

void movieEvent(Movie m) {
  m.read();
}
