<?php function displayHead($title = '', $ogImagePath = '' ) { ?>

<!doctype html>
<html lang="fr-FR" class="no-js">

<head>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NDS4C47');</script>
<!-- End Google Tag Manager -->

  <meta charset="utf-8">
  <title><?php echo $title; ?></title>
  <meta name="description" content="Expérimentations plastiques">
  <meta name="viewport" content="width=device-width">
  <meta property="og:image" content="<?php echo $ogImagePath; ?>"/>

  <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#92c3c8">
  <meta name="msapplication-TileColor" content="#92c3c8">
  <meta name="theme-color" content="#ffffff">

  <link rel="stylesheet"
        href="<?php echo ASSETS_PATH; ?>main.css?v=<?php echo filectime(SITE_ROOT_PATH . 'assets/main.css') ?>">
</head>

<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NDS4C47"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<!--[if lte IE 9]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade
  your browser</a> to improve your experience and security.</p>
<![endif]-->

<?php } ?>