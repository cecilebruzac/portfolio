<article class="references row row--centered">
  <h2 class="references__title text text--uppercase text--big text--bigger-md">Références artistiques</h2>
  <div class="row__col row__col--half-md row__col--with-right-gutter-md">
    <h3 class="references__subtitle text text--bold text--uppercase text--medium">Participation à des expositions collectives</h3>
    <ul>
      <li class="text text--medium">
        <em class="text--italic">Fenêtre sur rue • Paris-Cergy/Rouen</em>
        <br>
        Carte-vidéo blanche à Éric&nbsp;Maillet
        <br>
        Galerie Martainville de l‘ERBA de Rouen, 20 janvier - 5 février&nbsp;2011
      </li>
      <li class="text text--medium">
        Festival des <em class="text--italic">E-magiciens</em><br>
        Théâtre Le Phénix, Valenciennes, 1-3 décembre&nbsp;2010
      </li>
      <li class="text text--medium">
        <em class="text--italic">Dé-synchronisation</em><br>
        Commissaires : Alice Pereira, Jin Kyung&nbsp;Lee<br>
        Espace des Arts Sans Frontières, Paris, 4-17 mai&nbsp;2010
      </li>
      <li class="text text--medium">
        <em class="text--italic">Kiss Me Deadly</em>.<br>
        Commissaires : Éric Maillet, Jeff Guess, Judith&nbsp;Perron<br>
        Mains d’œuvres, Saint-Ouen, 16 juin 2009<br>
        <a class="text__link" href="http://galerie.chez-robert.com/expositions/expo-kmd" target-="_blank">chez-robert</a>, 16 juin - 9 juillet&nbsp;2009
      </li>
      <li class="text text--medium">
        <em class="text--italic">OrsolTM</em> / <em class="text--italic">Chapitre 6</em><br>
        Commissaires : Mathilde Villeneuve, Bernard&nbsp;Marcadé<br>
        Abbaye de Maubuisson, Saint-Ouen-L’Aumône, 6-29 juin&nbsp;2009
      </li>
      <li class="text text--medium">
        Festival des <em class="text--italic">Bains Numériques#4</em><br>
        Centre François Villon, Enghien-les-Bains, 5-13 juin&nbsp;2009
      </li>
      <li class="text text--medium">
        <em class="text--italic">Kiss My Super Hero</em><br>
        Commissaires : Pierre Daugy, Capucine&nbsp;Vever<br>
        Arslonga, Paris, 5 février&nbsp;2009
      </li>
      <li class="text text--medium">
        <em class="text--italic">Supertremplin</em><br>
        La Vitrine, Paris, 16 décembre&nbsp;2008
      </li>
      <li class="text text--medium">
        Festival des <em class="text--italic">Bains Numériques#3</em><br>
        Marché couvert d’Enghien-les-Bains, 6 juin&nbsp;2008
      </li>
    </ul>
  </div><!--
  --><div class="row__col row__col--half-md row__col--with-left-gutter-md">
    <h3 class="references__subtitle text text--bold text--uppercase text--medium">Cursus en écoles d’art</h3>
    <ul>
      <li class="text text--medium">
        DNSEP de l’ENSA de Paris-Cergy en 2009, avec les félicitations du&nbsp;jury.
      </li>
      <li class="text text--medium">
        DNAP de l’ESA de Lorient en 2007, avec&nbsp;mention.
      </li>
    </ul>
    <h3 class="references__subtitle text text--bold text--uppercase text--medium">Expériences en tant que développeuse/programmeuse</h3>
    <ul>
      <li class="text text--medium">Conception et programmation Java d&rsquo;un générateur de patron de montage pour l’œuvre <em class="text--italic">Avalanche</em>
        d’Évariste Richer, en septembre&nbsp;2012.
      </li>
      <li class="text text--medium">Programmation de l’œuvre <em class="text--italic">Reworks</em> d’Alexis Guillier, pour son exposition dans le Module Hors les Murs
        du Palais de Tokyo en avril&nbsp;2010.
      </li>
      <li class="text text--medium">Conception et programmation d&rsquo;un logiciel générateur de vidéographies pour animer les scènes de concerts
        de Laurent Garnier. Travail mené avec Camille Baudelaire, de décembre 2009 à février&nbsp;2010.
      </li>
      <li class="text text--medium">Participation au cycle supérieur de recherche Image Temps Réel à l’ENSADLab pour la conception et mise en
        œuvre de dispositifs interactifs, de septembre 2009 à février&nbsp;2010.
      </li>
      <li class="text text--medium">Participation à la conception et développement de sites web, templates de newsletter sur mesure pour des institutions culturelles, au cours d’un CDI dans l’agence <a class="text__link" href="https://residence-mixte.com/" target="_blank">Résidence Mixte</a>.<br>
      Traits plastiques, newsletter du CNAP • 
      <a class="text__link" href="https://tram-idf.fr/" target="_blank">site web de TRAM, réseau art contemporain Paris / Île-de-France</a> • 
      <a class="text__link" href="http://esadmm.fr/" target="_blank">site web de l’École supérieure d’art & de design Marseille-Méditerranée</a> • 
      <a class="text__link" href="http://aureldesignurbain.fr/" target="_blank">web portfolio de l’agence Aurel design urbain</a> • 
      sites de La nuit européenne des musées 2014 et des Rendez-vous aux jardins 2013-2014 commandités par le ministère de la Culture • 
      <a class="text__link" href="https://viavoxproduction.com/" target="_blank">site web de Viavox Production</a>, &hellip;
      </li>
    </ul>
  </div>
</article>