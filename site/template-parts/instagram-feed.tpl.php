<?php if(!count($data)) die; ?>

<ul class="instagram-feed" id="instagram-feed"><!--
  <?php
  $username = $data[0]['username'];
  ?>
  --><li class="instagram-feed__item follow-me">
    <div class="follow-me__inner">
      <div class="follow-me__content">
        <p class="text text--medium text--bold">
          <a class="follow-me__link"
             href="https://www.instagram.com/cecilebruzacdessine"
             target="_blank">
            <img class="follow-me__avatar"
                 src="<?php echo ASSETS_PATH; ?>images/avatar.jpg"
                 alt="Cécile Bruzac"
                 width="60"><!--
            --><img class="follow-me__instagram-logo"
                    src="<?php echo ASSETS_PATH; ?>images/instagram.svg"
                    height="40" width="40"
                    alt="instagram">
          </a>
        </p>
      </div>
    </div>
  </li><!--
  <?php
  foreach ($data as $item) {
    $caption = $item['caption'];
    $link = $item['permalink'];
    $src = $item['media_url'];;

    $timestamp = new DateTime($item['timestamp']);
    $date = $timestamp->format('d/m/Y');
  ?>
  --><li class="instagram-feed__item">
    <a class="instagram-feed__link"
       href="<?php echo $link; ?>"
       target="_blank">
      <div class="instagram-feed__inner">
        <img class="instagram-feed__picture instagram-feed__picture--hidden"
             data-src="<?php echo $src; ?>"
             alt="<?php echo $caption; ?>">
        <div class="instagram-feed__caption">
          <p class="instagram-feed__caption-text text text--small">
            <?php echo $date; ?>
            <br>
            <span class="text--bold"><?php echo $caption; ?></span>
          </p>
        </div>
      </div>
    </a>
  </li><!--
  <?php } ?>
--></ul>