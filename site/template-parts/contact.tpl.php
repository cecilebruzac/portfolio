<div class="contact row row--centered">
  <div class="contact__bloc row__col row__col--third-md row__col--half-lg row__col--with-right-gutter-md">
    <p class="text text--medium">© 2021 cecilebruzac.fr</p>
  </div><!--
  --><div
      class="contact__bloc row__col row__col--third-md row__col--quarter-lg row__col--with-left-gutter-md row__col--with-right-gutter-md">
    <p class="text text--medium">
      <a class="text__link"
         href="mailto:&#099;&#111;&#110;&#116;&#097;&#099;&#116;&#064;&#099;&#101;&#099;&#105;&#108;&#101;&#098;&#114;&#117;&#122;&#097;&#099;&#046;&#102;&#114;">&#099;&#111;&#110;&#116;&#097;&#099;&#116;&#064;&#099;&#101;&#099;&#105;&#108;&#101;&#098;&#114;&#117;&#122;&#097;&#099;&#046;&#102;&#114;</a>
      <br>
      9 rue de Madagascar
      <br>
      75012 PARIS
    </p>
  </div><!--
  --><div class="contact__bloc row__col row__col--third-md row__col--quarter-lg row__col--left-gutter-md">
    <p class="text text--medium">
      <a class="text__link"
         href="https://gitlab.com/cecilebruzac"
         target="_blank">GitLab</a>
    </p>
    <p class="text text--medium">
      <a class="text__link"
         href="https://www.linkedin.com/in/cbruzac"
         target="_blank">LinkedIn</a>
    </p>
    <p class="text text--medium">
      <a class="text__link"
         href="https://twitter.com/cecilebruzac"
         target="_blank">Twitter</a>
    </p>
  </div>
  <p class="text text--small"><a class="text__link" href="/legals">Mentions légales</a></p>
</div>