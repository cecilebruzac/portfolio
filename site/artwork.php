<?php
  require_once('config.php');
  require_once TEMPLATE_PARTS_PATH . 'head.tpl.php';
  $title = 'Recherches artistiques | Cécile Bruzac';
  $ogImagePath = '';
  displayHead($title, $ogImagePath);
?>

<header class="main-header">
  <h1 class="main-header__title text text--bold text--big"><a class="text__link" href="/">Cécile Bruzac</a></h1>
</header>

<section class="artwork">
  <div class="artwork__projects projects">
    <ul class="projects__list">
      <li class="projects__list-item project">
        <figure>
          <div class="video-wrapper video-wrapper--dark">
            <video id="expanded-video"
                   class="with-limited-height" 
                   width="1728" height="1080"
                   poster="<?php echo ASSETS_PATH; ?>images/video-elargie-poster.jpg"
                   onclick="this.paused ? this.play() : this.pause();"
                   onplay="removeElement('play-expanded-video-button')"
                   loop muted>
              <source src="<?php echo ASSETS_PATH; ?>images/video-elargie.mov" type="video/mp4">
            </video>
            <button class="video-wrapper__play-button" id="play-expanded-video-button" onClick="playVideo('expanded-video')">Play</button>
          </div>
          <figcaption class="project__caption row row--centered">
            <div class="row__col row__col--half-md row__col--with-right-gutter-md">
              <p class="text text--medium">Génération d’un montage de plusieurs occurences d’une même capture vidéo dont les temps de lecture sont décalés pour former un moving panorama&nbsp;extensible.</p>
              <p class="text text--small">Cergy, 2007.</p>
              <p class="text text--small">Logiciel développé avec Max/Jitter<br>
                <a class="text__link"
                   href="https://gitlab.com/cecilebruzac/expanded-video"
                   target="_blank">
                  <svg viewBox="0 0 22 22" width="22" height="22"
                       role="img">
                    <title>vpl</title>
                    <use xlink:href="<?php echo ASSETS_PATH; ?>images/defs.svg#source-code"></use>
                  </svg>
                </a>
              </p>
            </div>
          </figcaption>
        </figure>
      </li>
      <li class="projects__list-item project">
        <figure class="row row--centered">
          <div class="row__col row__col--half-md row__col--with-right-gutter-md">
            <img src="<?php echo ASSETS_PATH; ?>images/flip-boucle.jpg"
               width="800"
               height="800"
               alt="flip boucle">
            <figcaption class="project__caption">
              <p class="text text--medium">Vidéo exportée image par image, imprimée et reliée en&nbsp;boucle.</p>
              <p class="text text--small">Travelling sur le paysage saisi depuis une nacelle de la grande&nbsp;roue.</p>
              <p class="text text--small">Paris, 2007, ré-édité en 2009.</p>
            </figcaption>
          </div>
        </figure>
      </li>
      <li class="projects__list-item project">
        <figure>
          <div class="video-wrapper video-wrapper--dark">
            <img class="with-limited-height" src="<?php echo ASSETS_PATH; ?>images/videoprojection.jpg">
          </div>
          <figcaption class="project__caption row row--centered">
            <div class="row__col row__col--half-md row__col--with-right-gutter-md">
              <p class="text text--medium">DNSEP à l’ENSAPC 2009</p>
              <p class="text text--small">&copy; Kyrill Charbonnel</p>
            </div>
          </figcaption>
        </figure>
      </li>
      <li class="projects__list-item project">
        <figure class="row row--centered row--with-right-aligned-col">        
          <div class="row__col row__col--half-md row__col--with-left-gutter-md">
            <div class="video-wrapper">
              <video id="bande-optico-sonique-video"
                     width="1072" height="1800"
                     poster="<?php echo ASSETS_PATH; ?>images/bande-optico-sonique.png"
                     loop
                     onclick="this.paused ? this.play() : this.pause();"
                     onplay="removeElement('play-bande-optico-sonique-video-button')">
                <source src="<?php echo ASSETS_PATH; ?>images/bande-optico-sonique.mov" type="video/mp4">
              </video>
              <button class="video-wrapper__play-button" id="play-bande-optico-sonique-video-button" onClick="playVideo('bande-optico-sonique-video')">Play</button>
            </div>
            <figcaption class="project__caption">
              <p class="text text--medium">Génération d’une bande son à partir d’une vidéo montrant le paysage à travers la fenêtre d’un&nbsp;train. La ligne d’horizon, représentée dans la vidéo, est interprétée comme profil&nbsp;musical.</p>
              <p class="text text--small">Logiciel développé avec Processing en 2008-2009, à&nbsp;Cergy.<br>
                <a class="text__link"
                   href="/bande-optico-sonique.pde"
                   target="_blank">
                  <svg viewBox="0 0 22 22" width="22" height="22"
                       role="img">
                    <title>vpl</title>
                    <use xlink:href="<?php echo ASSETS_PATH; ?>images/defs.svg#source-code"></use>
                  </svg>
                </a>
              </p>
            </figcaption>
          </div>
        </figure>
      </li>
      <li class="projects__list-item project">
        <figure>
          <div class="video-wrapper video-wrapper--dark">
            <video id="stabilisation-video" 
                   class="with-limited-height" width="1728" height="1080"
                   poster="<?php echo ASSETS_PATH; ?>images/stabilisation-poster.jpg"
                   onclick="this.paused ? this.play() : this.pause();"
                   onplay="removeElement('play-stabilisation-video-button')"
                   loop muted>
              <source src="<?php echo ASSETS_PATH; ?>images/stabilisation.mov" type="video/mp4">
            </video>
            <button class="video-wrapper__play-button" id="play-stabilisation-video-button" onClick="playVideo('stabilisation-video')">Play</button>
          </div>
          <figcaption class="project__caption row row--centered">
            <div class="row__col row__col--half-md row__col--with-right-gutter-md">
              <p class="text text--medium">Génération de la stabilisation de la vidéo.</p>
              <p class="text text--small">Lomener / Paris, 2009.</p>
              <p class="text text--small">Logiciel développé avec Max/Jitter.<br>
                <a class="text__link"
                   href="https://gitlab.com/cecilebruzac/stabilisation"
                   target="_blank">
                  <svg viewBox="0 0 22 22" width="22" height="22"
                       role="img">
                    <title>vpl</title>
                    <use xlink:href="<?php echo ASSETS_PATH; ?>images/defs.svg#source-code"></use>
                  </svg>
                </a>
              </p>
            </div>
          </figcaption>
        </figure>
      </li>
      <li class="projects__list-item project">
        <figure>
          <div class="ribbon">
            <div class="ribbon__wrapper">
              <img src="<?php echo ASSETS_PATH; ?>images/performance/performance-00.jpg" alt="performance 0"><img src="<?php echo ASSETS_PATH; ?>images/performance/performance-01.jpg" alt="performance 1"><img src="<?php echo ASSETS_PATH; ?>images/performance/performance-02.jpg" alt="performance 2"><img src="<?php echo ASSETS_PATH; ?>images/performance/performance-03.jpg" alt="performance 3"><img src="<?php echo ASSETS_PATH; ?>images/performance/performance-04.jpg" alt="performance 4"><img src="<?php echo ASSETS_PATH; ?>images/performance/performance-05.jpg" alt="performance 5"><img src="<?php echo ASSETS_PATH; ?>images/performance/performance-06.jpg" alt="performance 6"><img src="<?php echo ASSETS_PATH; ?>images/performance/performance-07.jpg" alt="performance 7"><img src="<?php echo ASSETS_PATH; ?>images/performance/performance-08.jpg" alt="performance 8"><img src="<?php echo ASSETS_PATH; ?>images/performance/performance-09.jpg" alt="performance 9"><img src="<?php echo ASSETS_PATH; ?>images/performance/performance-10.jpg" alt="performance 10"><img src="<?php echo ASSETS_PATH; ?>images/performance/performance-11.jpg" alt="performance 11"><img src="<?php echo ASSETS_PATH; ?>images/performance/performance-12.jpg" alt="performance 12"><img src="<?php echo ASSETS_PATH; ?>images/performance/performance-13.jpg" alt="performance 13"><img src="<?php echo ASSETS_PATH; ?>images/performance/performance-14.jpg" alt="performance 14"><img src="<?php echo ASSETS_PATH; ?>images/performance/performance-15.jpg" alt="performance 15"><img src="<?php echo ASSETS_PATH; ?>images/performance/performance-16.jpg" alt="performance 16"><img src="<?php echo ASSETS_PATH; ?>images/performance/performance-17.jpg" alt="performance 17"><img src="<?php echo ASSETS_PATH; ?>images/performance/performance-18.jpg" alt="performance 18"><img src="<?php echo ASSETS_PATH; ?>images/performance/performance-19.jpg" alt="performance 19"><img src="<?php echo ASSETS_PATH; ?>images/performance/performance-20.jpg" alt="performance 20"><img src="<?php echo ASSETS_PATH; ?>images/performance/performance-21.jpg" alt="performance 21">
            </div>
          </div>
          <figcaption class="project__caption row row--centered">
              <div class="row__col row__col--half-md row__col--with-right-gutter-md">
                <p class="text text--medium">Génération d’une représentation graphique à partir d’une performance vidéo sur la plage du Fort‑Bloqué.</h1>
                <p class="text text--small">Logiciel développé avec Processing en&nbsp;2010.</p>
              </div><!--
              --><div class="row__col row__col--half-md row__col--with-left-gutter-md">
                <p class="text text--small">Je maintiens fermement la caméra face à la mer de telle sorte que la ligne d’horizon traverse le champ en son milieu. Malgré mes efforts pour neutraliser mon corps, celui-ci bouge relativement. Le cadrage est instable. Lorsque mes forces s’épuisent et que je ne peux tenir davantage la posture, l’objectif s’incline vers le sol et je cesse alors de filmer.</p>
                <p class="text text--small">Un dispositif informatique génère un document durant l’expérience. La caméra étant directement connectée à un ordinateur au cours de la performance, le flux vidéo peut être traité en temps réel. Un logiciel découpe une selection verticale, mesurant un pixel de large, au centre de chaque dernière image saisie. Toutes les bandes sont successivement affichées dans un nouvel espace de dessin. Une forme spatio-temporelle est ainsi progressivement produite à la manière des slit-scan et photo-finish.</p>
                <p class="text text--small">La ligne d’horizon trace un graphique. Les variations de cette courbe informe de l’évolution de mes mouvements tout au long de la performance. Le tracé s’élève lorsque mon maintient faibli. À la fin de la performance, la ligne d’horizon fuit via la bordure supérieure de l’image.</p>
              </div>
          </figcaption>
        </figure>
      </li>
    </ul>
  </div>

  <div class="artwork__content">
    <div class="row row--centered">
      <h2 class="artwork__title text text--alt-color text--uppercase text--big text--bigger-md">À propos</h2>
      <div class="artwork__intro row__col row__col--two-thirds-md">
        <p class="text text--alt-color text--medium text--bold-sm text--big-sm">Mes recherches plastiques se développent à partir d’études sur les dispositifs culturels d’apparition de l’image. Via ma pratique de la programmation, de la vidéo et du dessin, j’expérimente de nouvelles formes de représentation du&nbsp;paysage.</p>
        <p class="text text--alt-color text--medium text--bold-sm text--big-sm">Je m’intéresse aux études physiologiques, aux sciences cognitives, aux inventions des machines de vision qui préfigurent celle du cinéma, aux ordinateurs, aux théories du paysage et de&nbsp;l’horizon.</p>
      </div>
    </div>
    <?php include TEMPLATE_PARTS_PATH . 'references.tpl.php'; ?>
  </div>
</section>

<?php include TEMPLATE_PARTS_PATH . 'footer.tpl.php'; ?>





