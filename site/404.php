<?php
  require_once('config.php');
  require_once TEMPLATE_PARTS_PATH . 'head.tpl.php';
  $title = '404 | Cécile Bruzac';
  $ogImagePath = ASSETS_PATH . 'images/trace-thumb.svg';
  displayHead($title, $ogImagePath);
?>

<header class="main-header">
  <p class="main-header__title text text--bold text--big"><a class="text__link" href="/">Cécile Bruzac</a></p>
</header>
<section class="error">
  <h1 class="text text--bold text--bigger">404</h1>
  <p class="text text--big"><a class="text__link" href="/"> &larr; </a></p>
</section>

<?php include TEMPLATE_PARTS_PATH . 'footer.tpl.php'; ?>