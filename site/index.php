<?php
  require_once('config.php');
  require_once TEMPLATE_PARTS_PATH . 'head.tpl.php';
  $title = 'Cécile Bruzac';
  $ogImagePath = ASSETS_PATH . 'images/trace-thumb.svg';
  displayHead($title, $ogImagePath);
?>
<section class="home">
  <header class="main-header home__main-header">
    <h1 class="main-header__title text text--bold text--big">Cécile Bruzac</h1>
  </header>

    <picture class="home__cover">
      <source media="(max-width: 46.875em)"
              sizes="100vw"
              srcset="assets/images/desk-sm.jpg 750w, assets/images/desk-sm@2x.jpg 1500w">
      <source media="(min-width: 46.875em)"
              sizes="100vw"
              srcset="assets/images/desk.jpg 1920w, assets/images/desk@2x.jpg 3840w">
      <img src="assets/images/desk.jpg"
           alt="Bureau" nopin="nopin" />
    </picture>

    <div class="home__content-wrapper">
      <article class="home__content">
        <div class="row row--centered">
          <p class="text text--big"><a class="text__link" href="/artwork"> &larr; Voir les anciens projets&nbsp;art&nbsp;+&nbsp;code</a></p>
        </div>
      </article>
      <ul class="home__instagram-feed" id="instagram-feed" data-instagram-token="<?php echo INSTAGRAM_ACCESS_TOKEN; ?>" data-instagram-feed-template="<?php echo BASEURL ?>instagram-feed.php" data-instagram-feed-id="instagram-feed"></ul>
    </div>
</section>

<?php include TEMPLATE_PARTS_PATH . 'footer.tpl.php'; ?>